# GameFAQs Solarized

A [Usercss](https://github.com/openstyles/stylus/wiki/Usercss) theme for [GameFAQs](https://gamefaqs.gamespot.com), using the colour palette from [Solarized](https://ethanschoonover.com/solarized/) (plus some additional colours for a few special cases).

[![Install directly with Stylus](https://img.shields.io/badge/Install%20directly%20with-Stylus-00adad.svg)](https://gitlab.com/maxigaz/gamefaqs-solarized/raw/master/gamefaqs-solarized.user.css)

## Install

1. Get a style manager. The style has been tested with [Stylus](https://add0n.com/stylus.html), although it is also compatible with [xStyle](https://github.com/FirefoxBar/xStyle). Other style managers might also work but are *not* supported.
2. Install the stylesheet by clicking [here](https://gitlab.com/maxigaz/gamefaqs-solarized/raw/master/gamefaqs-solarized.user.css) and a new window of Stylus should open, asking you to confirm to install the style.
3. Enjoy.

## Options

Currently, the following options are available in case you want to customise the style:

- Switch between Solarized Dark and Solarized Light (affects the background and text colours)
- Ability to override the colour for borders
- Image brightness:
	- Specified in percentage. 80 by default so that it’s easier on the eye in the dark.
	- Regardless of this setting, when you hover the mouse over an image, the value always becomes 100.

## Reporting issues

Before you open a new issue ticket, please, make sure you’ve installed the **latest development version**. (Open the style from [here](https://gitlab.com/maxigaz/gamefaqs-solarized/raw/master/gamefaqs-solarized.user.css). If you have installed it earlier, tell Stylus to reinstall it, overriding the version you currently have.)

In the issue description, include the following:

- An example URL to the page you’re experiencing the problem on or provide step by step instructions on how to reproduce it.
- The version of your web browser, style manager, and the userstyle.
- In addition, including a screenshot or screencast of the problem is also helpful.

## Changelog

Changes between releases are summarized [here](https://gitlab.com/maxigaz/gamefaqs-solarized/blob/master/CHANGELOG.md).

## Credits

In almost all cases, the colour palette from [Solarized](https://ethanschoonover.com/solarized/) has been followed as an example.

## More userstyles from me

If you like this userstyle, check out the list of all styles created/maintained by me [here](https://gitlab.com/maxigaz/userstyles).
