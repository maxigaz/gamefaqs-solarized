# Changelog

## v0.4.0

- Styled Q&A pages
- Styled tables
- Styled most of user ratings on game summary page

## v0.3.1

- Apply adjustable setting for border colour in the advanced search table

## v0.3.0

- Styled basic and advanced search pages

## v0.2.0

- Added styles for forums threads

## v0.1.0

Initial release
